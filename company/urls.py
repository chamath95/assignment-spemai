from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('company', views.CompanyList.as_view(), name='company_list'),
    path('saveCompany', views.saveCompany, name='company_list'),
    path('deleteCompany/<int:id>', views.deleteCompany, name='company_list'),
    path('media', views.saveCompany, name='company_list')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
