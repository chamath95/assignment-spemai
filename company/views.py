from django.shortcuts import render,redirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Company
from django.views.generic import ListView, DetailView
from django.conf import settings
from django.core.files.storage import FileSystemStorage

# Create your views here.


def home(request):
    return render(request, 'home.html')


def saveCompany(request):
    if request.method == "POST":
        name = request.POST['name']
        website = request.POST['website']
        logo = simple_upload(request)
        email = request.POST['email']

        if name and website and logo and email:
            company = Company.objects.create(
                name=name,
                website=website,
                email=email,
                logo=logo
            )
            company.save()

    return redirect('/company')


def deleteCompany(request, id):
    instance = Company.objects.get(id=id)
    instance.delete()
    return redirect('/company')

def simple_upload(request):
    try:
        if request.FILES['logo']:
            myfile = request.FILES['logo']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            uploaded_file_url = fs.url(filename)

            return uploaded_file_url

        else:
            return ''
    except:
        return ''


class CompanyList(ListView):
    model = Company


